---
layout: post
title: "Gaining or losing? What is good?"
author: "Santhosh George"
date: "Sept 3, 2021"
read_time: "2 min"
img: "/assets/images/b1.webp"
---

Gaining is good or losing is good? It’s a tricky question I often ask children. The answer is it all depends on what you gain or what you lose. Gaining wisdom, confidence and a good reputation is truly good. But imagine if someone is gaining unhealthy weight? It’s good or bad? It’s truly bad because this gain is bringing some loss that is called health! 


Gaining unhealthy weight results in losing health and gaining health means you are losing unhealthy weight. There are certain things that are good to lose and one among them is the unhealthy weight. This is very important when it comes to children gaining unhealthy weight at an early stage. The tragedy is that many children and their parents don’t even realize the fact that they are losing their precious health as they gain unhealthy weight. This gaining and losing occurrence needs immediate serious response.


The World Health Organization (WHO) declared obesity as a disease. Obesity is a disease, because it has consequences if not treated well soon. Also since obesity is a disease, it can be treated like any other illness. However if neglected, obesity can bring severe health issues including diabetes, cholesterol, hypertension that ultimately leads to deadly stroke and heart related emergencies. Unfortunately many parents ignore the unhealthy weight among their children merely because of their ignorance. Parents also ignore, because most often, neither they know the reason for their children gaining weight nor they know how to respond to the unhealthy weight their children are gaining.


Taking this reality into serious consideration, the HealthAshram’s mission is to spread awareness on this silent yet salient phenomenon among children. The task is not just raising awareness. The real task is to share wisdom among children and their parents so that millions of children can be saved from this tragedy of gaining unhealthy weight. HealthAshram lifestyle helps children stay fit all through their lives. Simple changes in the quality and quantity of food and active life activities can ensure good health to anyone.  


I have come across many people glorifying obesity. I am not condemning obesity either. I know for some children it is not over eating or sedentary life that has brought unhealthy weight in them. It could be some disorder or imbalance that has caused this condition. But too much glorification of obesity is unwise and this will lead to many children acquiring chronic illness from a very early stage in life.

<img height="500" src="{{site.baseurl}}{{page.img}}">

Gaining is good as long as you are gaining health and losing is good as long as you are losing unhealthy weight. What is much better is not to gain unhealthy weight throughout your childhood and continue to sustain health and healthy lifestyle till old age as long as you live. Welcome to HealthAshram lifestyle and start a new wise life where you can gain health and lose unhealthy weight.


Santhosh George

[HealthAshram.in]({{site.baseurl}})