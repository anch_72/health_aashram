---
layout: blog
title: "Every food is a processed food!"
author: "Santhosh George"
last_modified_at: "Sep 3, 2021"
date: "Sept 2, 2021"
read_time: "4 min"
img: "/assets/images/b2.webp"
---

**One student asked me if we, at the HealthAshram, eat processed food. My answer was YES! It was surprising for the child! And I explained to her my simple answer.**


I said we all eat processed soil and dirt and water all our life. But the question is who processed our food? Who processed soil and dirt and manure and water to make it our food? The answer is those plants and trees around us! We see beautiful and tasty mangoes and oranges and apples and gooseberries with different colour and flavour and good smell. Look at the gooseberries they are tiny nutrition bombs loaded with vitamin C and all goodness on Earth. Next time you see any fruit, remember that the mother trees have processed them for us. The gooseberry tree has processed it for us and gave it at the right season before the winter so that we have all the vitamin C that we require to face the harsh winter with ease.


The problem starts with the second and third time mechanical processing. Processing the natural fruits and nuts and vegetables with too much of salt and sugar and oil and fire, which not only take away all goodness from them but also take away good health from us. The greed and profit motive business minds introduces this processed and packed and preserved food that is brining so much of diseases and lifestyle illness among the present generation. Please remember that this large scale processing and aggressive marketing of such unhealthy food is a recent development. But unfortunately children of this millennium has only seen these bottled and colourful unnatural drinks and processed packed food all their lives. The young parents in your 30s and 40s can save the future of your children if you carefully bring the right food home. Stop living in ignorance and start living a wise life.


Be wise when it comes to what you put in your mouth and your digestive system. Don’t just be so foolish to ignore your naïve food habits and shopping behaviour. Take charge of your life and health and behaviour. Prepare a list before you go for your grocery purchase. Don’t just walk anywhere without applying your common sense and careful direction. Choose the place and choose the produce of the nature and not the products of the factory. Also look for natural alternatives. You decide if you want to drink aerated artificial coloured drinks or natural drinks like tender coconut, lemon juice, sugar cane juice and ginger tea etc.


Good news is that many families are adopting to the HealthAshram lifestyle and food habits to save their and their children’s good health. Once wise parents decide to live healthy change happens naturally, children adopt them naturally. You don’t bring what is not naturally processed to your home anymore. Your friends don’t bring too much of sweets and unnatural food as gifts once they know that you lead a better healthy lifestyle. You will soon become a role model for your colleagues and friends and family to teach them simple living. Simple living is living with the nature. Simple living is eating nature processed food and not factory processed food. Simple living is preserving the health of your organs and not spoiling your health and then work on reducing weight and reversing diabetes and cholesterol etc. Simple living is HealthAshram lifestyle. 


Now that you know that the processed food from nature is less harmful for our gut and digestive system, take control of your food habits. For your health sake, don’t fill your stomach with junk food and fast food. I have seen the so called ‘educated fools’ with too much of unhealthy weight and big bellies coming out of some big shops with loads of bottled aerated drinks. In winters they are sweating without any sensible reason. They did not walk or run to sweat like the way they do! That unnatural sweat is the result of over consumption of unnatural food and unnatural living style like broiler chickens. I feel so sad looking at children and adults who are carrying too much of unhealthy weight. I can understand some of them who are so unfortunate to have some health condition that resulted in their obesity. But if the health condition is a result of ignorance and lack of common sense in the food decisions, it’s time for immediate change. It’s time to wake up!


Start living like country chicken where you run and walk and play every day. Country chickens always wisely choose to eat only natural food. Unfortunately the cage bound broiler chicken on the other hand gets only those processed food the caretaker provide them. When it comes to your food, always make minimum alteration. Use less or no oil; use less salt and sugar. More importantly say not to mechanically processed food that has no goodness but only harmful ingredients. Remember always that the nature has already processed your food. Your job is to be thankful to those trees and plants and eat them as it is wisely. Every food is a processed food. But the question is who processed it for us? The nature or the factories!

<img height="500" src="{{site.baseurl}}{{page.img}}">

Santhosh George

HealthAshram
