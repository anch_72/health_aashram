---
layout: post
title: "Walking: A tested remedy for many ailments"
author: "Santhosh George"
date: "August 24, 2021"
read_time: "3 min"
img: "/assets/images/b4.jpg"
---

‘Walking for health’ may sound so simple! But walking has several health benefits.

When a child is born parents wait anxiously to see the first step their child takes. Its

celebration and joy to see children take their first step in their life. However in the

modern mechanical word, there are many obstacles to daily walking. Lifts in building,

vehicles in every house and reduces public foot paths are reasons for reduced

walking among majority of the people. Walking is also considered a poor man’s

means of communication and anyone who can afford take either public or private

transportation. Walking for retaining health and fitness is in the decline. This reduced

walking and the modern lifestyle unfortunately invites several lifestyle diseases.

Walking should be an experience everyone should have every day as long as they

live. We have to make walking an everyday essential health activity.

“Walking is mother of all exercises”. Walking is an activity that help our body

remain strong and in good shape. Walking is healing and walking is essential as long

as a person lives. Human body is built in such a way that simple walking adds

extraordinary benefits to our body, mind and soul. Walking oxygenate our body.

Walking help in digestion and take complete advantage of the food we consume.

Walking is healing. Walking cleans our blood. Walking enable our body to produce

happiness enzymes. Walking gives fresh positive thoughts and ideas. Walking help

us to live every day! When it comes to fitness, there is no alternate to walking. So

why wait? Start walking every day!

Walking in fact guarantees not only physical health but also peace of mind and

enthusiasm. Walking is not just for physical benefit. Walking also generates

happiness enzymes that sustain emotional and mental health. Walking takes away

depression and guarantees happiness because of the anti-depressant enzymes that

our body generates when we walk for 45 to 60 minutes. Unfortunately these natural

happiness enzymes are sold as anti-depressant medicines in pharmacies. Naturally

produced enzyme is much better than chemical based anti-depressant medicines.

Many new members of HealthAshram ask me, ‘why walk’? I give them the following

reasons so that they understand the importance of walking and start walking every

day. First, 65% of the blood in our body is stored in our thigh and when we walk, this

blood gets shaken and a process of blood circulation starts all over the body.

Walking sustain this active flow of blood and thus clearing all blockades and

cleansing and purifying our body. Second, walking helps our heart and lungs remain

healthy and functional. Thirdly, walking creates movement of all major bones and

thus feed the bones naturally.

Finally its walking that ensures oxygenation of our cells. Oxygenated blood from the

lungs is pumped by our heart (mechanical) to all the body cells through artery.

Oxygenated blood gives new life to our body cells. Now the question is how does the


deoxygenated blood return to the lungs? It’s important that on a regular basis, the

deoxygenated blood has to return the lungs so that the blood can be oxygenated

again. Here is where the role of manual hearts is significant. We have one

mechanical/ automatic heart that keep pumping blood from the lungs to all over the

body. And we have two manual hearts located in the calf. These manual hearts start

functioning only when we walk. For every step that we take, the heart in the calf

shrinks and sucks blood and sent them back to the lungs through our veins. If

anyone is not walking, this very important functioning of the manual hearts and

pumping of the deoxygenated blood back to the lungs do not happen. So the more

we walk the more oxygenated blood we will have all though our body. This regular

oxygenation helps in enhancing immunity, anti-ageing and maintaining good energy

level at all times.

Walking in an open park with trees and green around is better than walking on a

machine in a closed room. When we are in the open with the nature, it is not just the

walking and the internal actions that take place our mind also gets rejuvenated with

the sight of greens and the sound of birds and animals. When we are out in the

nature walking, new ideas and thought come fresh every day. Walking is healing and

it is remedy for several ailment including back pain, leg pain and indigestion etc.

HealthAshram is promoting walking as a regular activity among all age groups. The

intention of this promotion is to help all maintain their healthy weight before they lose

it. The general trend today is that people out of ignorance gain unhealthy weight and

then out of desperation do everything on weight loss. Walking has no alternate

action. Walking is a complete action. Walking is not an action only just few do.

Walking is so good and everyone should walk everyday as along as one is able.

<img height="500" src="{{site.baseurl}}{{page.img}}">

Santhosh George

HealthAshram

